<?php


if ( ! function_exists( 'inquiryhub_setup' ) ) :
function inquiryhub_setup() {

	add_theme_support( 'post-formats', array( 'image','quote','video' ) );

	add_theme_support('post-thumbnails');

	add_theme_support( 'title-tag' );

	if ( ! isset( $content_width ) ) {
		$content_width = 520;
		}

}
endif;
add_action( 'after_setup_theme', 'inquiryhub_setup' );


function inquiryhub_scripts() {

	wp_enqueue_script( 'masonry' );

	wp_enqueue_script("jquery");

	wp_enqueue_script( 'leafletjs-js', get_template_directory_uri() . '/leaflet/leaflet.js' );
	wp_enqueue_style( 'leafletjs-css', get_template_directory_uri() . '/leaflet/leaflet.css' );

	wp_enqueue_script( 'inquiryhub-mainjs', get_template_directory_uri() . '/js/main.js', array('jquery','masonry'), 1, true );

	}
add_action( 'wp_enqueue_scripts', 'inquiryhub_scripts' );


register_nav_menu('header',"Header Links");
register_nav_menu('homeslider',"Home Slider");
register_nav_menu('homelinks',"Home Links");



add_editor_style("css/editor-style.css");

// Callback function to insert 'styleselect' into the $buttons array
function my_mce_buttons_2( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
// Register our callback to the appropriate filter
add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );

function my_mce_before_init_insert_formats( $init_array ) {
	$style_formats = array(
		array(
			'title' => '.doshow',
			'block' => 'span',
			'classes' => 'doshow',
			'wrapper' => true
			),
		array(  
			'title' => '.blockarea',  
			'block' => 'div',  
			'classes' => 'blockarea pad1',
			'wrapper' => true,
			),
		array(  
			'title' => '.block',  
			'block' => 'div',  
			'classes' => 'block',
			'wrapper' => true,
			),
		array(  
			'title' => '.fullpost',  
			'block' => 'div',  
			'classes' => 'fullpost',
			'wrapper' => true,
			),
		array(  
			'title' => '.tile',  
			'block' => 'div',  
			'classes' => 'tile',
			'wrapper' => true,
			),
		array(  
			'title' => '.tile_half',  
			'block' => 'div',  
			'classes' => 'tile tile_half',
			'wrapper' => true,
			),
		array(  
			'title' => '.tile_third',  
			'block' => 'div',  
			'classes' => 'tile tile_third',
			'wrapper' => true,
			),
		array(  
			'title' => '.tile_fourth',  
			'block' => 'div',  
			'classes' => 'tile tile_fourth',
			'wrapper' => true,
			),
		array(  
			'title' => '.smalltile',  
			'block' => 'div',  
			'classes' => 'tile smalltile',
			'wrapper' => true,
			),
		array(  
			'title' => '.tile_c_full',  
			'block' => 'div',  
			'classes' => 'tile tile_c_full',
			'wrapper' => true,
			),
		array(  
			'title' => '.tile_c_half',  
			'block' => 'div',  
			'classes' => 'tile tile_c_half',
			'wrapper' => true,
			),
		);

	$init_array['style_formats'] = json_encode( $style_formats );  
	
	return $init_array;  
  
} 
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );



function inquiryhub_getmenuitems($menu_name) {

	if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
		$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );

		$menu_items = wp_get_nav_menu_items($menu->term_id);

		return $menu_items;
		}
	else {
		return array();
		}
	}

function inquiryhub_colours($colour) {
	if ($colour==="orange") { $colour="#fea74c"; }
	if ($colour==="green") { $colour="#55BD7A"; }
	if ($colour==="darkblue") { $colour="#001454"; }
	return $colour;
	}

?>
