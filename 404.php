<?php
	get_header();
?>

	<div id='pagecontent'>

		<div class='tile_c_full'>
			<div class='pad15'>

				<h2>404s are the worst, aren't they?</h2>

				<br/>
				<h3>But if you're a student and you're actively <i>looking</i>
				for the 404 page on a website... well, you're the <i>exact</i>
				sort of person we want at iHub.
				<a href='http://inquiryhub.org/apply'>Here's the link to
				apply</a>.</h3>

			</div>
		</div>

<?php
	get_footer();
?>
