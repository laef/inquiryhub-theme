(function($) {


//$("#header_spacer").css("height",$("#header").height()+"px");

	function inquiryhub_doscroll() {
		var scroll = $(window).scrollTop();
		if (scroll<10) {
			$("#header").css("box-shadow","none");
			$("#header_margins").css("margin","10px");
			$("#header .logo").css("height","50px").css("width","50px");
			$("#header img.userop").css("margin-top","15px");
			$(".headlink").css("margin-top","-10px").css("margin-bottom","-10px")
				.css("height","73px").css("line-height","75px").css("font-size","20px");
			}
		else {
			$("#header").css("box-shadow","0px 0px 10px rgba(0, 0, 0, 0.29)");
			$("#header_margins").css("margin","5px");
			$("#header .logo").css("height","30px").css("width","30px");
			$("#header img.userop").css("margin-top","3px");
			$(".headlink").css("margin-top","-5px").css("margin-bottom","-5px")
				.css("height","43px").css("line-height","45px").css("font-size","15px");
			}
		}
	inquiryhub_doscroll();
	$(window).scroll(inquiryhub_doscroll);

})( jQuery );

function sidebar() {
	jQuery("#sidebar, #sidebarbackground").addClass("visible");
	jQuery("#s").attr("placeholder","Type here...");
	jQuery("#s").focus();
	}
function rmsidebar() {
	jQuery("#sidebar, #sidebarbackground").removeClass("visible");
	jQuery("#s").blur();
	}


window.onload = function(){

	console.log(
		" ___                   _            _   _       _     \n"+
		"|_ _|_ __   __ _ _   _(_)_ __ _   _| | | |_   _| |__  \n"+
		" | || '_ \\ / _` | | | | | '__| | | | |_| | | | | '_ \\ \n"+
		" | || | | | (_| | |_| | | |  | |_| |  _  | |_| | |_) |\n"+
		"|___|_| |_|\\__, |\\__,_|_|_|   \\__, |_| |_|\\__,_|_.__/ \n"+
		"              |_|             |___/                   ");

	console.log("\n\nIf you're a student and you're looking at your browser's"+
		" web console... well, you're the exact sort of person we want at iHub."+
		" Apply at inquiryhub.org/apply.\n\n");

	}
