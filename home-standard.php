<?php
	get_header();
?>

	<div id='pagecontent'>

		<div id='homeslideshow'>
			<div id='homeslideshow_content'>
				<div id='homeslideshow_text'>
					<h2>Inquiry Hub is an award-winning public secondary
					 school in Coquitlam</h2>
					<br/><br/>
					<h1>Where students get grants and build gardens.</h1>
				</div>
			</div>
		</div>

		<!--<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>
		  <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="post-header">
				<div class="date"><?php the_time( 'M j, Y' ); ?></div>
				<h2><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
			</div>
			<div class="entry clear">
				<?php if ( function_exists( 'add_theme_support' ) ) the_post_thumbnail(); ?>
				<?php the_content(); ?>
				<?php edit_post_link(); ?>
				<?php wp_link_pages(); ?>
			</div>
			<div class="post-footer">
				
			</div>
		  </div>
		<?php endwhile; /* rewind or continue if all posts have been fetched */ ?>
		  <div class="navigation index">
			<div class="alignleft"><?php next_posts_link( 'Older Entries' ); ?></div>
			<div class="alignright"><?php previous_posts_link( 'Newer Entries' ); ?></div>
		  </div>
		<?php else : ?>
		<?php endif; ?>-->

		<?php if ( have_posts() ) : ?>
		<div class="pad10"><div class="tiles">

		<?php while ( have_posts() ) : the_post(); ?>
			<a href="<?php the_permalink(); ?>" class='noshow'>
			<div class="tile tile_third"><div id='post-<?php the_ID(); ?>' class="<?php
				$classes = get_post_class();
				$classes[] = "p-asthumb";
				$classes[] = "mar10";
				$classes[] = "pad0";
				$classes[] = "hoverable";
				if (!has_post_thumbnail()) {
					$classes[] = "bg-orange";
					}
				$i = 0;
				while ($i < count($classes)) {
					if ($i>0) { echo(" "); }
					echo($classes[$i]);
					$i++;
					}
				?>">
				<?php if(has_post_thumbnail()) {
					the_post_thumbnail(
						array(400,250)
						);
					}
				else {
					echo("<br/><br/><br/>");
					} ?>
				<div class='mar15'>
				<h2><?php the_title(); ?></h2>
				<?php
				//if (!has_post_thumbnail()) {
					echo("<div style='height:10px;'></div>".get_the_excerpt());
					//the_excerpt();
				//	}
				?>
				</div>
				<?php if (!has_post_thumbnail()) {
					echo("<br/><br/><br/>"); } ?>
			</div></div>
			</a>
		<?php endwhile; /* rewind or continue if all posts have been fetched */ ?>

		  <div class="navigation index">
			<div class="alignleft"><?php next_posts_link( 'Older Entries' ); ?></div>
			<div class="alignright"><?php previous_posts_link( 'Newer Entries' ); ?></div>
		  </div><!--end navigation-->

		</div></div>
		<?php else : ?>
		<?php endif; ?>

	<?php
		wp_add_inline_script("inquiryhub-mainjs",
			"jQuery('.tiles').masonry({".
				"itemSelector: '.tile',".
				"columnWidth: '.tile_third',".
				"percentPosition: true".
				"})");
	?>

<?php
	get_footer();
?>
