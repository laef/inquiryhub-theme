			<?php $fields = get_post_custom($post->ID); ?>

			<a href="<?php the_permalink(); ?>" class='noshow'>
			<div class="tile tile_third"><div id='post-<?php the_ID(); ?>' class="<?php
				$classes = get_post_class();
				$classes[] = "p-asthumb";
				$classes[] = "mar10";
				$classes[] = "pad0";
				$classes[] = "hoverable";
				if (!has_post_thumbnail()) {
					$classes[] = "bg-orange";
					}
				$i = 0;
				while ($i < count($classes)) {
					if ($i>0) { echo(" "); }
					echo($classes[$i]);
					$i++;
					}
				?>" <?php
					if (isset($fields['colour_title'])) {
						echo("style='background-color:".inquiryhub_colours($fields['colour_title'][0]).";color:white;'");
						}
					?>>
				<?php if(has_post_thumbnail()) {
					the_post_thumbnail(
						array(400,250)
						);
					}
				else {
					echo("<br/><br/><br/>");
					} ?>
				<div class='mar15'>
				<h2><?php the_title(); ?></h2>
				<?php
				//if (!has_post_thumbnail()) {
					echo("<div style='height:10px;'></div>".get_the_excerpt());
					//the_excerpt();
				//	}
				if (isset($fields['event_start'])) {
					echo("<div style='height:10px;'></div>");
					$dmy = date_i18n("l, F j \a\\t g:ia",$fields['event_start'][0]);
					$event_status = 0;
					if (time()>$fields['event_start'][0]) {
						$event_status = 2;
						if (isset($fields['event_end'])) {
							if (time()<$fields['event_end'][0]) {
								$event_status = 1;
								}
							}
						}
					if ($event_status==0) { echo("<b>$dmy</b>"); }
					else if ($event_status==1) { echo("$dmy<br/><b>Happening now</b>"); }
					else { echo("$dmy<br/><b>Concluded</b>"); }
					}
				?>
				</div>
				<?php if (!has_post_thumbnail()) {
					echo("<br/><br/><br/>"); } ?>
			</div></div>
			</a>

