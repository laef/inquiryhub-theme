<?php
	get_header();

	if (isset($GLOBALS['ihub_page'])) {
		$ihub_page = $GLOBALS['ihub_page'];
		}
	else {
		$ihub_page = "normal";
		}
?>

	<div id='pagecontent'>


		<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<?php
			if (is_page("students")) {
				echo("<div style='background-image:url(".get_template_directory_uri().
					"/img/content/garden-sunflowers-blur.png);background-position:center center;".
					"background-size:cover;'>".
					"<div class='tac' style='background-color:rgba(0,0,0,0.1);color:white;".
						"text-shadow:0px 0px 5px rgba(0, 0, 0, 0.5);'>".
					"<br/><h2>".get_the_title()."</h2>");
				}
			else {
				if (has_post_thumbnail()) {
					$img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'thumbnail');
					$width = $img[1];
					$height = $img[2];
					$hpc = 100 / ($width/$height);
					?>
					<div style="background-image:url(<?php
						echo(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())));
						?>);background-position:center center;background-size:cover;">
						<div class="tac" style='background-color:rgba(0,0,0,0.2);color:white;' id="header_img_aspect">
							<div class='pad15'>
							<h2><?php the_title(); ?></h2>
							<br/><br/>
							</div>
						</div>
					</div>
					<style>
						#header_img_aspect{
							position: relative;
							}
						#header_img_aspect:before{
							content: "";
							display: block;
							padding-top: <?php echo($hpc);?>%;
							}
						#header_img_aspect>div {
							position:  absolute;
							top: 50%;
							left: 0;
							bottom: 0;
							right: 0;

							text-shadow: 0px 0px 5px rgba(0, 0, 0, 0.5);
							}
					</style>
				<?php }
				else { ?>
					<div class="pad15 bg-green tac">
						<h2><?php the_title(); ?></h2>
					</div><!--end post header-->
				<?php }
				}
			?>

			<?php
			if (is_page("contact")) { 
				echo("<div class='blockarea pad5'>".
					"<div class='tile tile_half'><div class='block mar10'>".
						"<h3>Inquiry Hub is a distinctly innovative program and we're".
							" always delighted to answer any questions or inquiries".
							" you might have.</h3><br/>".
						"<h3>Phone: <a href='tel:16049364285'>604-936-4285</a></h3>".
						"<h3>Email: <a href='mailto:admin@inquiryhub.org'>admin@".
						"inquiryhub.org</a></h3>".
						"<br/>".
						"Inquiry Hub's lead administrator is David Truss, our vice".
						" principal. Our principal is Michael McGlenen.".
					"</div>".
					"<div class='block mar10 tac'>".
						"<h2>Want to become a student at Inquiry Hub?</h2><br/>".
						"<h3><a href='http://inquiryhub.org/apply' class='input_button'>Apply Here</a></h3>".
					"</div></div>".
					"<div class='tile tile_half'><div class='block mar10'>".
						"<div id='ihubmap' class='topimage bg-green' style='height:300px;'></div>".
						"<h3>1432 Brunette Ave, Coquitlam, BC V3K 1G5</h3>".
						"<small>Entrance and parking off of Schoolhouse.</small>".
					"</div></div>".
				"</div>");
				wp_add_inline_script("inquiryhub-mainjs",
					"window.onload = function(){".
						"var ihubmap = L.map('ihubmap').setView([49.240238,-122.853337], 13);".
						"L.tileLayer('https://stamen-tiles.a.ssl.fastly.net/toner/{z}/{x}/{y}.png', {".
							"attribution: 'Map tiles by <a href=\"http://stamen.com\">Stamen Design</a>, under <a href=\"http://creativecommons.org/licenses/by/3.0\">CC BY 3.0</a>. Data by <a href=\"http://openstreetmap.org\">OpenStreetMap</a>, under <a href=\"http://www.openstreetmap.org/copyright\">ODbL</a>.',".
							"maxZoom: 18,".
							"}).addTo(ihubmap);".
						"var ihubmarker = L.marker([49.240238,-122.853337]).addTo(ihubmap);".
						"};");
				}
			?>

			<?php if ($ihub_page==="centered") { echo("<div class='tile_c_full'>"); } ?>
		  <div id="post-<?php the_ID(); ?>" class="<?php
				$classes = get_post_class();
				//$classes[] = 'block';
				$classes[] = 'pad15';
				$i = 0;
				while ($i < count($classes)) {
					if ($i>0) { echo(" "); }
					echo($classes[$i]);
					$i++;
					}
				?>" style='padding-top:5px;'>
			<div class="entry clear">
				<?php //if ( function_exists( 'add_theme_support' ) ) the_post_thumbnail(); ?>
				<?php the_content(); ?>
				<div class='tac'><?php edit_post_link(); ?></div>
				<?php wp_link_pages(); ?>
			</div><!--end entry-->
			<div class="post-footer">
				
			</div><!--end post footer-->
		  </div><!--end post-->
			<?php if ($ihub_page==="centered") { echo("</div>"); } ?>
		<?php endwhile; /* rewind or continue if all posts have been fetched */ ?>

		<?php else : ?>
		<?php endif; ?>

		<?php
			if (is_page("students")) {
				echo("</div></div>");
				}
		?>

	<?php
		if (is_page("students")) {
			$gslug = get_category_by_slug('student-work');

			$query = get_posts(array(
				'numberposts'=>-1,
				'category'=>$gslug->cat_ID
				));
			$projects = array();

			if ($query) {
				foreach ($query as $tpost) {
					$projects[] = $tpost;
					}
				shuffle($projects);
				}

			if (count($projects)>0) { ?>
				<div class="blockarea pad10 tac">
				<!--<h2 class="mar10">Upcoming Events</h2>-->
				<div class="tiles">
				<?php
				$bg = "green";
				foreach ($projects as $post ) {
					setup_postdata( $post );
					get_template_part( 'theloop', 'thirds' );
					}
				?>
				</div>
				</div>

				<?php
					wp_add_inline_script("inquiryhub-mainjs",
						"window.onload = function(){".
							"jQuery('#blogposts').masonry({".
								"itemSelector: '.tile',".
								"columnWidth: '.tile_third',".
								"percentPosition: true".
								"})".
							"};");
				 }?>
			<br/><br/>
			<?php wp_reset_postdata();
			}
	?>

	<br/><br/>

<?php
	get_footer();
?>
