<?php
	get_header();
?>

	<div id='pagecontent'>

		<div class="tiles" id='blogposts'>

		<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>
		  <?php get_template_part( 'theloop', 'thirds' ); ?>
		<?php endwhile; /* rewind or continue if all posts have been fetched */ ?>
			</div>
			<br class='cb' /><br/>
			<?php get_template_part( 'theloop', 'navigation' ); ?>
			<br class='cb' />
		<?php else : ?>
		<?php endif; ?>

		</div>

	<?php
		wp_add_inline_script("inquiryhub-mainjs",
			"window.onload = function(){".
				"jQuery('#blogposts').masonry({".
					"itemSelector: '.tile',".
					"columnWidth: '.tile_third',".
					"percentPosition: true".
					"})".
				"};");
	?>

<?php
	get_footer();
?>
