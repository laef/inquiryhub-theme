<!DOCTYPE html>

<html <?php language_attributes(); ?> class="no-js">

<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>" />

	<title><?php wp_title('-',true,'right'); ?>Inquiry Hub<?php if (is_home()) { echo(" Secondary School"); }?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<link rel='stylesheet' href="<?php bloginfo('stylesheet_url');?>" type='text/css' media='all' />

	<?php
		$twitter = array(
			"card"=>"summary",
			"site"=>"@inquiryhub",
			"title"=>"Inquiry Hub Secondary School",
			"description"=>"Inquiry Hub is Coquitlam's award-winning, inquiry-based, public secondary school.",
			"image"=>get_template_directory_uri()."/img/ihub-social-logo.png"
			);
		if (is_single()||is_page()) {
			$pid = get_the_ID();
			$twitter['title'] = get_the_title($pid);
			if (strlen(get_the_excerpt($pid))>0) {
				$twitter['description'] = get_the_excerpt($pid);
				}
			if (has_post_thumbnail($pid)) {
				$img = wp_get_attachment_image_src(
					get_post_thumbnail_id($pid),'single-post-thumbnail');
				if (isset($img['url'])) {
					$twitter['card'] = 'summary_large_image';
					$twitter['image'] = $img['url'];
					}
				}
			}
		if (is_category("events")) {
			$twitter['title'] = "Upcoming Events at Inquiry Hub";
			$twitter['description'] = "See what's coming up at Coquitlam's".
				" award-winning, inquiry-based, public secondary school.";
			}
		echo("<meta name=\"twitter:card\" content=\"".$twitter['card']."\" />".
			"<meta name=\"twitter:site\" content=\"".$twitter['site']."\" />".
			"<meta name=\"twitter:title\" content=\"".esc_attr($twitter['title'])."\" />".
			"<meta name=\"twitter:description\" content=\"".esc_attr($twitter['description'])."\" />".
			"<meta name=\"twitter:image\" content=\"".esc_attr($twitter['image'])."\" />");

		echo("<meta name=\"og:site_name\" content=\"Inquiry Hub\" />".
			"<meta name=\"og:title\" content=\"".esc_attr($twitter['title'])."\" />".
			"<meta name=\"og:description\" content=\"".esc_attr($twitter['description'])."\" />".
			"<meta name=\"og:image\" content=\"".esc_attr($twitter['image'])."\" />");

		echo("<meta name=\"Description\" content=\"".esc_attr($twitter['description'])."\" />");

/*
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@flickr" />
<meta name="twitter:title" content="Small Island Developing States Photo Submission" />
<meta name="twitter:description" content="View the album on Flickr." />
<meta name="twitter:image" content="https://farm6.staticflickr.com/5510/14338202952_93595258ff_z.jpg" />
*/

	?>

	<?php wp_head(); ?>

</head>


<body <?php body_class(); ?>>

	<?php get_sidebar(); ?>

	<div id='header'><div id='header_margins'>

		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class='noshow'><img class="logo" src="<?php echo(get_template_directory_uri().'/img/logo.svg');?>" /></a>

		<!--<a href='?p=thestudents' class='headlink'>The Students</a>
		<a href='?p=theschool' class='headlink'>The School</a>
		<a href='?p=news' class='headlink'>News</a>
		<a href='?p=contact' class='headlink'>Apply / Contact</a>-->
		<span class='min900'><?php
			/*wp_nav_menu(array(
				"theme_location"=>"header"
				));*/
			$headlinks = inquiryhub_getmenuitems("header");
			foreach ( (array) $headlinks as $key => $menu_item ) {
				//print_r($menu_item);
				echo("<a class='headlink' href='".$menu_item->url."'>".
					$menu_item->title."</a>");
				}
		?></span>

		<div id='userops'>
			<img src="<?php echo(get_template_directory_uri().'/img/menu.png');?>" class='userop' onclick='sidebar();' />
		</div>

	</div></div>

	<div id='header_spacer'></div>
