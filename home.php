<?php
	get_header();
?>

	<div id='pagecontent'>

		<?php if (!is_paged()) { ?>

			<?php
			$menuitems = inquiryhub_getmenuitems("homeslider");
			$homesliders = array();
			foreach ( (array) $menuitems as $key => $menu_item ) {
				$pid = (int)$menu_item->object_id;
				$fields = get_post_custom($pid);
				
				if (isset($fields['homeslider_wheretext'])) {
					$homesliders[] = array(
						"wheretext"=>$fields['homeslider_wheretext'][0],
						"url"=>$menu_item->url,
						"img"=>wp_get_attachment_url(get_post_thumbnail_id($pid))
						);
					}

				//print_r($menu_item);

				/*echo("<a class='headlink' href='".$menu_item->url."'>".
					$menu_item->title."</a>");*/
				}
			shuffle($homesliders);
			if (count($homesliders)>0) {
				$homeslider = $homesliders[0];
				?><a href="<?php echo($homeslider['url']);?>" class='noshow'>
				<div id='homeslideshow' <?php
						echo("style=\"background-image:url(".$homeslider['img'].");\"");
						?>>
					<div id='homeslideshow_content'>
						<div id='homeslideshow_text'>
							<h2>Inquiry Hub is an award-winning public secondary
							 school in Coquitlam</h2>
							<br/><br/>
							<h1 id='homeslideshow_wheretext'><?php
								echo($homeslider['wheretext']);
							?></h1>
						</div>
					</div>
				</div>
				</a><?php
				}
			?>


			<div class="tac bg-darkgreen" id='homelinks'><?php

			$menuitems = inquiryhub_getmenuitems("homelinks");
			$bg = "orange";
			foreach ( (array) $menuitems as $key => $menu_item ) {
				/*echo("<br/><br/>");
				print_r($menu_item);*/
				if ($bg=="orange") { $bg="green"; }
				else { $bg = "orange"; }
				echo("<a href='".$menu_item->url."' class='noshow tal'".
					" target='".$menu_item->target."'>".
					"<div class='tile tile_fourth bg-$bg hoverable'>".
					"<div class='pad15'>".
						"<h3><b>".$menu_item->title."</b></h3>".
						"<div style='height:10px;'></div>".
						$menu_item->post_content.
					"</div></div></a>");

				//print_r($menu_item);

				/*echo("<a class='headlink' href='".$menu_item->url."'>".
					$menu_item->title."</a>");*/
				}

			?></div>

			<?php
				wp_add_inline_script("inquiryhub-mainjs",
					"jQuery(function(){".
						"height = 0;".
						"jQuery('#homelinks .tile').each(function(index){".
							"if (jQuery(this).height()>height) {".
								"height = jQuery(this).height();".
								"}".
							"});".
						"jQuery('#homelinks .tile').css('height',(height+1)+'px');".
						"});");
			?>




			<?php
				$gslug = get_category_by_slug('events');

				$query = get_posts(array(
					'numberposts'=>-1,
					'category'=>$gslug->cat_ID
					));
				$events = array();

				if ($query) {
					foreach ($query as $tpost) {
						$fields = get_post_custom($tpost->ID);
						if (isset($fields['event_start'])) {
							$usetime = $fields['event_start'][0];
							if (isset($fields['event_end'])) {
								$usetime = $fields['event_end'][0];
								}
							if ($usetime>time()) {
								$events[] = array("post"=>$tpost,"fields"=>$fields);
								}
							}
						}
					usort($events,function($a,$b){
						$a = $a['fields']['event_start'][0];
						$b = $b['fields']['event_start'][0];
						if ($a==$b) { return 0; }
						return ($a < $b) ? -1 : 1;
						});
					}

				if (count($events)>0) { ?>
					<div class="pad10 tac">
					<h2 class="mar10">Upcoming Events</h2>
					<div class="tiles">
					<?php
					$bg = "green";
					foreach ($events as $event ) {
						if ($bg=="orange") { $bg="green"; }
						else { $bg = "orange"; }

						$post = $event['post'];
						setup_postdata( $post );
						//the_post(); ?>
						<a href="<?php the_permalink(); ?>" class='noshow'>
						<div class="tile smalltile"><div id='post-<?php the_ID(); ?>' class="<?php
							$classes = get_post_class();
							$classes[] = "p-asthumb";
							$classes[] = "mar10";
							$classes[] = "hoverable";
							$classes[] = "bg-$bg";
							$i = 0;
							while ($i < count($classes)) {
								if ($i>0) { echo(" "); }
								echo($classes[$i]);
								$i++;
								}
							?>">
							<h2><?php the_title();?></h2>
							<?php the_excerpt();?>
							<b><?php
							echo(date_i18n("D, F j @ g:ia",$event['fields']['event_start'][0]));
							?></b>
						</div></div>
						</a>
					<?php }?>
					<?php
					wp_add_inline_script("inquiryhub-mainjs",
						"jQuery(function(){".
							"height = 0;".
							"jQuery('.tiles .smalltile .post').each(function(index){".
								"if (jQuery(this).height()>height) {".
									"height = jQuery(this).height();".
									"}".
								"});".
							"jQuery('.tiles .smalltile .post').css('height',(height+1)+'px');".
							"});");
					?>
					</div>
					<p><a href="?cat=<?php echo($gslug->cat_ID);?>">More events</a></p>
					</div>
				<?php }?>
			<br/><br/>
			<?php wp_reset_postdata(); ?>
		<?php }?>



		<div class='blockarea'>
		<?php if ( have_posts() ) : ?>
		<div class="pad10"><h2 class="mar10 tac"><?php
			if (is_paged()) {
				echo("Read. Think. Learn.</h2><div class='tac'>Page ");
				if ( get_query_var( 'paged' ) ) { $paged = get_query_var( 'paged' ); }
				elseif ( get_query_var( 'page' ) ) { $paged = get_query_var( 'page' ); }
				else { $paged = 1; }
				echo($paged."</div>");
				}
			else {
				echo("Dream. Create. Learn.</h2>");
				}
		?><br/>

		<div class="tiles" id='blogposts'>

		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'theloop', 'thirds' ); ?>
		<?php endwhile; /* rewind or continue if all posts have been fetched */ ?>
		</div>
		<br class='cb' /><br/>
		<?php get_template_part( 'theloop', 'navigation' ); ?>
		<br class='cb' />

		</div>
		<?php else : ?>
		<?php endif; ?>
		</div>

	<?php
		wp_add_inline_script("inquiryhub-mainjs",
			"window.onload = function(){".
				"jQuery('#blogposts').masonry({".
					"itemSelector: '.tile',".
					"columnWidth: '.tile_third',".
					"percentPosition: true".
					"})".
				"};");
	?>

	<br/><br/>

<?php
	get_footer();
?>
