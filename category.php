<?php
	get_header();
?>

	<div class="pad15 bg-orange tac"><h2><?php single_cat_title();?></h2></div>

	<div id='pagecontent'><div class='blockarea'>

		<?php if ( have_posts() ) : ?>
		<div class="pad10">
		<div class="tiles" id='blogposts'>
		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'theloop', 'thirds' ); ?>
		<?php endwhile; /* rewind or continue if all posts have been fetched */ ?>
		</div>
		<br class='cb' /><br/>
		  <?php get_template_part( 'theloop', 'navigation' ); ?>
		</div>
		<?php else : ?>
		<?php endif; ?>

	<?php
		wp_add_inline_script("inquiryhub-mainjs",
			"jQuery('#blogposts').masonry({".
				"itemSelector: '.tile',".
				"columnWidth: '.tile_third',".
				"percentPosition: true".
				"})");
	?>
	<br/></div><br/>

<?php
	get_footer();
?>
