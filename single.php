<?php
	get_header();
?>

		<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post();
			$format = get_post_format();
			$fields = get_post_custom();

			$content = get_the_content();
			$match = array();
			preg_match_all('#\bhttps://(www\.)?youtube\.com\/[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/))#',
				$content, $match);
			if (isset($match[0][0])) {
				//echo($match[0][0]);
				echo("<div style='background-color:black;text-align:center;'>".
					"<iframe class='videothing'".
						" src='".str_replace("watch?v=","embed/",$match[0][0])."?autoplay=true'". 
						" frameborder='0' allowfullscreen></iframe>".
					"</div>");
				}


			?>

			<?php if ($format==="video") {?>
				<div class="pad15 bg-orange tac" <?php
						if (isset($fields['colour_title'])) {
							echo("style='background-color:".inquiryhub_colours($fields['colour_title'][0]).";'");
							}
						?>>
					<h2><?php the_title(); ?></h2>
					<div class="date"><?php the_time( 'M j, Y' ); ?></div>
				</div>
			<?php } ?>

			<div id="pagecontent" class="<?php
				//if ($format!='video') { echo('mar10'); }
				?>">

			<div class="blockarea pad1">

			<?php
			$fields = get_post_custom($post->ID);
			$cats = get_the_category();
			$catslugs = array();
			foreach ($cats as $cat) {
				$catslugs[] = $cat->slug;
				}
			?>
		  <div class="tile_c_full" style='margin-top:20px;'>
			<div id="post-<?php the_ID(); ?>" class="<?php
				$classes = get_post_class();
				//$classes[] = "p-single";
				$classes[] = "mar10";
				$i = 0;
				while ($i < count($classes)) {
					if ($i>0) { echo(" "); }
					echo($classes[$i]);
					$i++;
					}
				?>">
			<?php if ($format!="video") { ?>
				<div class="post-header p-single"<?php
						if (isset($fields['colour_title'])) {
							echo(" style='background-color:".inquiryhub_colours($fields['colour_title'][0]).";'");
							}
						?>>
					<?php if (!isset($fields['event_start'])) { ?>
						<div class="date"><?php the_time( 'M j, Y' ); ?></div>
					<?php }?>
					<h2><?php the_title(); ?></h2>
					<?php if (isset($fields['event_start'])) { ?>
						<div style='height:10px;'></div>
						<b><?php echo(date_i18n("l, F j \a\\t g:ia",$fields['event_start'][0]));?></b>
						<?php if (isset($fields['event_end'])) { ?>
							<br/>Goes until <?php echo(date_i18n("g:ia",$fields['event_end'][0]));?>
						<?php }
						 }?>
				</div><!--end post header-->
			<?php } ?>
			<div class="entry clear">
				<?php //if ( function_exists( 'add_theme_support' ) ) the_post_thumbnail(); ?>

				<?php 
					add_filter('the_content',function($string){
						$string = preg_replace("/<embed[^>]+>/i","",$string,1);
						return preg_replace(
							"/<iframe[^>]+>/i",
							"",$string,1);
						});
					the_content();
				?>
				<?php edit_post_link(); ?>
				<?php wp_link_pages(); ?>
			</div><!--end entry-->
			<div class="post-footer">
				
			</div><!--end post footer-->
		  </div></div><!--end post-->
		<div class='tac'>
			<?php
			if (in_array("events",$catslugs)) { 
				$gslug = get_category_by_slug('events');
				echo("<a href='?cat=".$gslug->cat_ID."' class='noshow'>".
					"<div class=\"tile tile_c_half\">".
					"<div class=\"block bg-orange mar10 hoverable\">".
					"<h2>Events</h2>See what's happening at Inquiry Hub in the".
					" next while.".
					"</div></div></a>");
				if (in_array("ihub-talks",$catslugs)) {
					$tpage = get_page_by_path("ihub-talks");
					echo("<a href='".get_permalink($tpage)."' class='noshow'>".
						"<div class=\"tile tile_c_half\">".
						"<div class=\"block bg-darkblue mar10 hoverable\">".
						"<h2>iHub Talks</h2>Inquiry Hub's student-organised".
						" series of topical speakers.".
						"</div></div></a>");
					}
				}
			?>
		</div></div>
		<?php endwhile; /* rewind or continue if all posts have been fetched */ ?>

		<?php else : ?>
			<div id="pagecontent" class="mar10">
		<?php endif; ?>

	<br/><br/>

<?php
	get_footer();
?>
