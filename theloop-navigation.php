		  <div class="navigation index tac">
			<div class="alignright"><?php previous_posts_link( '&laquo; More Recent' ); ?></div>
			<div><?php
				if ( get_query_var( 'paged' ) ) { $paged = get_query_var( 'paged' ); }
				elseif ( get_query_var( 'page' ) ) { $paged = get_query_var( 'page' ); }
				else { $paged = 1; }
				echo("Page ".$paged);
			?></div>
			<div class="alignleft"><?php next_posts_link( 'Earlier &raquo;' ); ?></div>
		  </div>
