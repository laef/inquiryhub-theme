	<p class='tac'><?php
		$phrases = array(
			"A school where opportunities allow the seed of an idea to blossom".
				" into a garden.",
			"This website is student-made, student-powered and student-driven.",
			"Dream. Create. Learn.",
			"Yes, we're definitely a normal school. Totally."
			);
		shuffle($phrases);
		echo($phrases[0]);

	?></p>

	<p class='tac'>
		<a href='https://facebook.com/inquiryhub' target='_blank' class="social_icon_link social_facebook">
			<img src="<?php echo(get_template_directory_uri());?>/img/entypo/facebook-with-circle.svg"
				class='social_icon' title='Facebook' alt='Facebook' /></a>
		<a href='https://twitter.com/inquiryhub' target='_blank' class="social_icon_link social_twitter">
			<img src="<?php echo(get_template_directory_uri());?>/img/entypo/twitter-with-circle.svg"
				class='social_icon' title='Twitter' alt='Twitter' /></a>
		<a href='https://www.youtube.com/user/InquiryHub' target='_blank' class="social_icon_link social_youtube">
			<img src="<?php echo(get_template_directory_uri());?>/img/entypo/youtube-with-circle.svg"
				class='social_icon' title='YouTube' alt='YouTube' /></a>
	</p>

	</div>

	<div class="pad15 cb" style='background-color:#e8e8e8;font-size:10px;text-align:center;'>Our School strives to keep our community informed. However, during urgent events, please refer to <a href='http://www.sd43.bc.ca' target='_blank'>sd43.bc.ca</a> to ensure you receive the most accurate and up-to-date information.</div>


</body>

<?php wp_footer(); ?>

</html>
